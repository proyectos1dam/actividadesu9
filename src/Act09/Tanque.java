package Act09;

public class Tanque{
    private int capacidad;
    private int carga;

    public Tanque(int capacidad) {
        this.capacidad = capacidad;
        this.carga = 0;
    }

    public void agregarCarga(int carga) throws TanquePleException {
        if (this.carga < carga) {
            this.carga += carga;
            throw new TanquePleException();
        }
    }

    public void retirarCarga(int carga) throws TanqueVacioException {
        if (this.carga == 0) {
            throw new TanqueVacioException();
        }
        this.carga -= carga;
    }
}
