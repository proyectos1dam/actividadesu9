package Act09;

public class Test {
    public static void main(String[] args) {
        Tanque tanque = new Tanque(1000);

        try {
            tanque.retirarCarga(50);
            tanque.agregarCarga(800);
            tanque.agregarCarga(300);
        } catch (TanqueVacioException e) {
            System.err.println(e.getMessage());
        } catch (TanquePleException ple){
            System.err.println(ple.toString());
        }
    }
}
