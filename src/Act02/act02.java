package Act02;

import java.util.InputMismatchException;
import java.util.Scanner;

public class act02 {
    public static void main(String[] args) {
        int max = 0;

        for (int i = 0; i < 6; i++) {
            try {
                int n = pedirEntero();
                if (n > max) {
                    max = n;
                }
            } catch (InputMismatchException e) {
                System.out.println("Debe introducir un número");
                i--;
            }
        }
        System.out.println("El mayor es el: " + max);
    }

    public static int pedirEntero() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Dame un número: ");
        int num = scanner.nextInt();
        return num;
    }
}
