package Act01;

import java.util.Scanner;

public class act02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int max = 0;

        for (int i = 0; i < 6; i++) {
            try {
                System.out.println("Dame un número: ");
                int num = Integer.parseInt(scanner.nextLine());
                if (num > max) {
                    max = num;
                }
            } catch (NumberFormatException e) {
                System.out.println("Debe introducir un número");
                i--;
            }
        }
        System.out.println("El mayor es el: " + max);
    }
}
