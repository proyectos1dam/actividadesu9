package Act01;

import java.util.InputMismatchException;
import java.util.Scanner;

public class act01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int max = 0;

        for (int i = 0; i < 6; i++) {
            try {
                System.out.println("Dame un número: ");
                int num = scanner.nextInt();
                if (num > max) {
                    max = num;
                }
            } catch (InputMismatchException e) {
                System.out.println("Debe introducir un número");
                scanner.next();
                i--;
            }
        }
        System.out.println("El mayor es el: " + max);
    }
}
