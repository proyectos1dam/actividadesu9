package Act07;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Act07 {
    public static void main(String[] args) {
        System.out.println("Hola bienvenido!!");
        try {
            int edad = getEdad();
            System.out.printf("Tienes %d años", edad);
        } catch (InputMismatchException e) {
            System.err.println(e.getMessage());
        }
    }

    private static int getEdad() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Dime tu edad: ");
        int edad = scanner.nextInt();
        if (edad < 10 || edad > 50) {
            throw new InputMismatchException("La edad está fuera de rango");
        } else if(edad > 50){
            throw new InputMismatchException("Eres muy anciano");
        } else if(edad < 18){
            throw new InputMismatchException("Eres un bebé");
        }
        return edad;
    }
}
