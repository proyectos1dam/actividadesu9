package Act08;

public class WaitApp {
    public static void main(String[] args) {
        try {
            waitSeconds(3);
        } catch (InterruptedException ex) {
            System.out.println("Error explicito");
        } catch (IllegalArgumentException iex) {
            System.out.println("Error implicito");
        }
    }

    private static void waitSeconds(int segons) throws InterruptedException {
        Thread.sleep(segons * 1000);
    }
}
